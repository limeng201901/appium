import os

from ui_framework.page.basepage import BasePage



class Login(BasePage):
   def login(self):
      dir=os.path.dirname(__file__)
      self.parse(dir + "/login.yaml", "myprofile")
      self.parse(dir + "/login.yaml", "account_login")
      self.parse(dir+"/login.yaml","name")
      self.parse(dir + "/login.yaml", "password")
      self.parse(dir + "/login.yaml", "login_btn")
      self.parse(dir + "/login.yaml", "setting")
      self.parse(dir + "/login.yaml", "logout")
      self.parse(dir + "/login.yaml", "yesout")





