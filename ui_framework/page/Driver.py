import yaml
from appium import webdriver

import os
dir = os.path.dirname(__file__)
with open(dir + "/../datas/caps.yml") as f:
    datas = yaml.safe_load(f)
    desires = datas['desirecaps']
    ip = datas['server']['ip']
    port = datas['server']['port']

class BuildUpDriver:
    driver= ''
    @staticmethod
    def build_up_driver():
         BuildUpDriver.driver = webdriver.Remote(f"http://{ip}:{port}/wd/hub", desires)
         print("kkkkkkkkkkkkk")
         print(type(BuildUpDriver.driver))

         BuildUpDriver.driver.implicitly_wait(5)