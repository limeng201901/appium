import os
import signal
import subprocess

import pytest


from ui_framework.page.logger import log_init
from ui_framework.page.basepage import BasePage



@pytest.fixture(scope="session", autouse=True)
def record():
    log_init()
    # 用例运行前做一些事情
    # cmd = "scrcpy -Nr tmp.mp4"
    # p = subprocess.Popen(cmd, shell=True)
    #BuildUpDriver.build_up_driver()
    # print("BuildUpDriver")
    # print(type(BuildUpDriver.driver))
    BasePage().__init__()




    yield

    # yield
    # # 用例运行后做一些事情
    # # os.kill(p.pid, signal.CTRL_C_EVENT)

